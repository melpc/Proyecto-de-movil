package pm.facci.alcivarjuan.thiefsselfie_1;

import android.content.DialogInterface;
import android.os.Bundle;
//import android.support.design.widget.FloatingActionButton;
//import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView textViewIntentos, textViewCorreo, textViewCarpeta, guardarCorreo, guardarCarpeta, guardarIntentos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textViewIntentos = (TextView) findViewById(R.id.textViewIntentos);
        textViewCorreo = (TextView) findViewById(R.id.textViewCorreo);
        textViewCarpeta = (TextView) findViewById(R.id.textViewCarpeta);
        guardarCarpeta = (TextView) findViewById(R.id.guardarCarpeta);
        guardarCorreo = (TextView) findViewById(R.id.guardarCorreo);
        guardarIntentos = (TextView) findViewById(R.id.guardarIntentos);

        final String[] listItems = getResources().getStringArray(R.array.itemsIntentos);

        textViewIntentos.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(final View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle(R.string.seleccion_intentos);
                builder.setSingleChoiceItems(listItems, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        guardarIntentos.setText(listItems[which]);
                    }

                });
                builder.setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton(R.string.cancelar, null);

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });


        textViewCorreo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                builder.setMessage(R.string.texto_correo);

                final EditText editTextCorreo = new EditText(MainActivity.this);
                builder.setView(editTextCorreo);

                builder.setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        guardarCorreo.setText(editTextCorreo.getText().toString());
                    }
                });

                builder.setNegativeButton(R.string.cancelar, null);

                builder.setView(editTextCorreo);
                builder.create().show();
            }
        });

        textViewCarpeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                builder.setMessage(R.string.texto_carpeta);

                final EditText editTextCarpeta = new EditText(MainActivity.this);
                builder.setView(editTextCarpeta);

                builder.setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        guardarCarpeta.setText(editTextCarpeta.getText().toString());
                    }
                });

                builder.setNegativeButton(R.string.cancelar, null);

                builder.setView(editTextCarpeta);
                builder.create().show();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_desinstalar) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
